import {toHTML} from '../../js/board/to-html';
describe('board-service', function() {

    it('should change text center to format html', function() {

        var text = '[c]ben[/c]';
        var expectText = '<center>ben</center>';
        var textResult = toHTML.toCenter(text);
        expect(textResult).toBe(expectText);
    });

    it('should change text bold to format html', function() {

        var text = '[b]ben[/b]';
        var expectText = '<b>ben</b>';
        var textResult = toHTML.toBold(text);
        expect(textResult).toBe(expectText);
    });

    it('should change text italic to format html', function() {

        var text = '[i]ben[/i]';
        var expectText = '<i>ben</i>';
        var textResult = toHTML.toItalic(text);
        expect(textResult).toBe(expectText);
    });

    it('should change text underline to format html', function() {

        var text = '[u]ben[/u]';
        var expectText = '<u>ben</u>';
        var textResult = toHTML.toUnderline(text);
        expect(textResult).toBe(expectText);
    });

    it('should change text hide to format html', function() {

        var text = '[hide]ben[/hide]';
        var expectText = '<section layout="column" ng-init="ctrl.buttonHides.hideId0=\'Show\'">'
                +'<md-button class="button-hide" ng-click="ctrl.actionHide(ctrl.buttonHides, \'hideId0\')"'
                +'>{{ctrl.buttonHides.hideId0}}</md-button><md-content '
                +'class="content-hide" ng-hide="ctrl.buttonHides.hideId0!=\'Hide\'">ben</md-content></section>';
        var textResult = toHTML.toHide(text);
        expect(textResult).toBe(expectText);
    });

    it('should change text youtube to format html', function() {

        var text = '[youtube]https://www.youtube.com/watch?v=YQHsXMglC9A[/youtube]';
        var expectText = '<center><div><embed width="600" height="471" '
            +'src="https://www.youtube.com/v/YQHsXMglC9A"></div></center>';
        var textResult = toHTML.toYoutube(text);
        expect(textResult).toBe(expectText);
    });

 });
