let mainController = ['$log', '$mdSidenav', '$state',
    function($log, $mdSidenav, $state){

        $log.log('[mainController] Start.');

        let ctrl = this;

        ctrl.init = () => {
            ctrl.listUpdate = [
                {
                    name: 'Event',
                    showContent: true,
                    data:[
                        {
                            header: 'Ben',
                            content: 'Ben'
                        },
                        {
                            header: 'Ben2',
                            content: 'Ben2'
                        }
                    ],
                    stateSeeAll: 'board'
                },
                {
                    name: 'Board',
                    showContent: true,
                    stateSeeAll: 'board'
                },
                {
                    name: 'Video/Music',
                    showContent: true,
                    stateSeeAll: 'board'
                }
            ];

            ctrl.menus = [
                {
                    name: 'Record',
                    icon: 'maps:ic_rate_review_24px',
                    state: 'board'
                },
                {
                    name: 'Chat',
                    icon: 'action:ic_question_answer_24px',
                    state: 'board'
                },
                {
                    name: 'Board',
                    icon: 'av:ic_library_books_24px',
                    state: 'board'
                },
                {
                    name: 'Music/Vedio',
                    icon: 'av:ic_music_video_24px',
                    state: 'board'
                },
                {
                    name: 'Game',
                    icon: 'hardware:ic_videogame_asset_24px',
                    state: 'board'
                },
                {
                    name: 'Business',
                    icon: 'maps:ic_store_mall_directory_24px',
                    state: 'board'
                }
            ];
        }

        ctrl.showMenu = () => {
            $mdSidenav('menu')
              .toggle();
        };

        ctrl.showContent = (index) => {
            ctrl.listUpdate[index].showContent = !ctrl.listUpdate[index].showContent;
        };

        ctrl.go = (state) => {
            $state.go(state);
        }

        $log.log('[mainController] End.');

    }];

export {
    mainController
}
