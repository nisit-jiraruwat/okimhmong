import angular from 'angular';

let main = angular.module('main', []);

import {mainController} from './main-controller';
main.controller('mainController', mainController);
