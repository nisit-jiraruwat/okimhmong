import angular from 'angular';

import 'angular-animate';
import 'angular-material';
import 'angular-route';
import 'angular-ui-router';

import './board/module';
import './main/module';
import './toolbar/module';

let myApp = angular.module('myApp', [
    'ngAnimate',
    'ngMaterial',
    'ui.router',
    'ngRoute',
    //
    'board',
    'main',
    'toolbar'
]);
/*
myApp.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;

            element.bind('change', function(e){
                scope.$apply(function(){
                    modelSetter(scope, e.target.files[0]);
                });
            });
        }
    };
}]);*/

myApp.config(['$urlRouterProvider', ($urlRouterProvider) => {
    $urlRouterProvider.otherwise('/');
}]);

myApp.config([
    '$stateProvider',
    '$locationProvider',
    ($stateProvider, $locationProvider) => {
        $stateProvider
            .state('otherwise', {
                url: '/',
                templateUrl: 'main.html',
                controller: "mainController as ctrl"
            })
            .state('board', {
                url: '/board',
                templateUrl: 'board.html',
                controller: 'boardController as ctrl'
            });
            //$locationProvider.html5Mode(true);
            $locationProvider.html5Mode({
                enabled: true,
                requireBase: false
            });

    }]);

myApp.config(['$mdIconProvider', ($mdIconProvider) => {
    $mdIconProvider
        .iconSet('action', './../icons/svg-sprite-action.svg')
        .iconSet('alert', './../icons/svg-sprite-alert.svg')
        .iconSet('av', './../icons/svg-sprite-av.svg')
        .iconSet('communication', './../icons/svg-sprite-communication.svg')
        .iconSet('content', './../icons/svg-sprite-content.svg')
        .iconSet('editor', './../icons/svg-sprite-editor.svg')
        .iconSet('file', './../icons/svg-sprite-file.svg')
        .iconSet('hardware', './../icons/svg-sprite-hardware.svg')
        .iconSet('image', './../icons/svg-sprite-image.svg')
        .iconSet('maps', './../icons/svg-sprite-maps.svg')
        .iconSet('navigation', './../icons/svg-sprite-navigation.svg')
        .iconSet('socail', './../icons/svg-sprite-socail.svg')
        .iconSet('toggle', './../icons/svg-sprite-toggle.svg');
}]);


myApp.config(function($mdThemingProvider) {
    $mdThemingProvider.theme('red')
    .primaryPalette('red',{'default':'900'})
});
