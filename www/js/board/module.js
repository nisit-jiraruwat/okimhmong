import angular from 'angular';

import './write/module';

let board = angular.module('board', [
    'board.write'
]);

import {Board} from './board-service';
board.factory('Board', Board);

import {boardController} from './board-controller';
board.controller('boardController', boardController);

import {toHtmlDirective} from './to-html-directive';
board.directive('toHtml', toHtmlDirective);
