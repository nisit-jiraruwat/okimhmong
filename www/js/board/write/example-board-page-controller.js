import {toHTML} from './../to-html';

let exampleBoardPageController = ['$log', '$mdDialog', 'title', 'text',
    function($log, $mdDialog, title, text){

        $log.log('[exampleBoardPageController] Start.');

        let ctrl = this;

        ctrl.init = () => {

            ctrl.buttonHides = {};

            ctrl.actionHide = (buttonHides, hide) => {
                if(buttonHides[hide] === 'Show'){
                    buttonHides[hide] = 'Hide';
                } else {
                    buttonHides[hide] = 'Show';
                }
            };

            ctrl.text = toHTML.to(text);
            ctrl.title = toHTML.toEntity(title);

            ctrl.cancel = () => {
                $mdDialog.cancel();
            };
        }

        $log.log('[exampleBoardPageController] End.');

    }];

export {
    exampleBoardPageController
}
