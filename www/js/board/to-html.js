
let to = () => {

    let countHideId = 0;
    let makeHideId = () => {
        countHideId++
        return 'hideId' + countHideId;
    };

    let toEntity = (text) => {
    	return text
    		.replace(/&/g, "&amp;")
    		.replace(/</g, "&lt;")
    		.replace(/>/g, "&gt;")
    		.replace(/"/g, "&quot;")
    		.replace(/'/g, "&#039;")
    		.replace(/\\/g, "&#92;");
    }

    let toCenter = (text) => {
        return text.replace(/\[c\]/g, '<center>')
                .replace(/\[\/c\]/g, '</center>');
    };

    let toBold = (text) => {
        return text.replace(/\[b\]/g, '<b>')
                .replace(/\[\/b\]/g, '</b>');
    };

    let toItalic = (text) => {
        return text.replace(/\[i\]/g, '<i>')
                .replace(/\[\/i\]/g, '</i>');
    };

    let toUnderline = (text) => {
        return text.replace(/\[u\]/g, '<u>')
                .replace(/\[\/u\]/g, '</u>');
    };

    let toHide = (text) => {
        let newText = toHideAll(text, 0);
        newText = newText.replace(/\[\/hide\]/g, '</md-content></section>');
        return newText;
    };

    let toHideAll = (text, id) => {
	    if(text.match(/\[hide\]/) != null) {
            let hide = 'hideId' + id;
            let textHide = '<section layout="column" ng-init="ctrl.buttonHides.' + hide
                    + '=\'Show\'"><md-button class="button-hide" '
                    +'ng-click="ctrl.actionHide(ctrl.buttonHides, \'' + hide
                    + '\')">{{ctrl.buttonHides.' + hide
                    + '}}</md-button><md-content class="content-hide"'
                    +' ng-hide="ctrl.buttonHides.' + hide
                    + '!=\'Hide\'">';
            let newText = text.replace('[hide]', textHide);
		    return toHideAll(newText, ++id);
	   }
       return text;
    };

    let toYoutube = (text) => {
    	let reg = /\[youtube\](?:https?:\/\/)?(?:www\.)?(?:youtube\.com(?:\/watch\?v=))([\w-]{11})\[\/youtube\]/g;
    	let urls =  text.match(reg);
    	if(urls == null) {
    		return text;
    	}
    	let newText = text;

    	for (let url in urls) {
    		let html = '<center><div><embed width="600" height="471" src="' +
                urls[url].replace('watch?v=','v/')
                .replace(/\[\/?youtube\]/g,'')
                + '"></div></center>';
		    newText = newText.replace(urls[url], html);
    	}
    	return newText;
    }

    let toImg = (text) => {
    	let reg = /\[img\](?:https?:\/\/)?(?:www\.)?[a-zA-Z0-9\-\/\.]+\.(?:png|gif|jpg)\[\/img\]/g;
    	let urls =  text.match(reg);
    	if(urls == null) {
    		return text;
    	}
    	let newText = text;

    	for (let url in urls) {
    		let html = '<center><div><img src="' + urls[url].replace(/\[\/?img\]/g,'') + '"/></div><center>';
		    newText = newText.replace(urls[url], html);
    	}
    	return newText;
    }

    let toImgList = (text) => {
    	let reg = /\[img-list\](\[img\](?:https?:\/\/)?(?:www\.)?[a-zA-Z0-9\-\/\.]+\.(?:png|gif|jpg)\[\/img\])*\[\/img-list\]/g;
    	let urls =  text.match(reg);
    	if(urls == null) {
    		return text;
    	}
    	let newText = text;

    	for (let url in urls) {
    		let html = urls[url].replace(/\[img-list\]/g,'<center><div class="img-list">').replace(/\[\/img-list\]/g,'</div></center>');
            html = toImgForList(html);
		    newText = newText.replace(urls[url], html);
    	}
    	return newText;
    }

    let toImgForList = (text) => {
    	let reg = /\[img\](?:https?:\/\/)?(?:www\.)?[a-zA-Z0-9\-\/\.]+\.(?:png|gif|jpg)\[\/img\]/g;
    	let urls =  text.match(reg);
    	if(urls == null) {
    		return text;
    	}
    	let newText = text;

    	for (let url in urls) {
    		let html = '<img class="list" src="' + urls[url].replace(/\[\/?img\]/g,'') + '" height="200" width="200"/>';
		    newText = newText.replace(urls[url], html);
    	}
    	return newText;
    }

    let toLink = (text) => {
        let reg = /\[link\](http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)\[\/link\]/g;
        let urls =  text.match(reg);
    	if(urls == null) {
    		return text;
    	}
    	let newText = text;

    	for (let url in urls) {
    		let html = '<a ng-href="' + urls[url].replace(/\[\/?link\]/g,'') + '" target="_blank"/>' + urls[url].replace(/\[\/?link\]/g,'') + '</a>';
		    newText = newText.replace(urls[url], html);
    	}
    	return newText
    };

    let toSelect = (text) => {
        return (toFunction) => {
            if(toFunction === undefined) {
                return text;
            }
            return toSelect(toFunction(text));
        }
    }

    let to = (text) => {
        return toSelect(text)(toEntity)(toUnderline)(toItalic)(toBold)(toCenter)(toHide)(toLink)(toYoutube)(toImgList)(toImg)();
    };

    return {
        toEntity,
        toCenter,
        toBold,
        toItalic,
        toUnderline,
        toHide,
        toYoutube,
        to
    };
};

let toHTML = to();

export {
    toHTML
}
