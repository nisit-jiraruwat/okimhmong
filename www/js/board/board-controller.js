import {toHTML} from './to-html';
import lodash from 'lodash';
let _ = lodash;

let boardController = ['$log', '$mdDialog', 'Board',
    function($log, $mdDialog, Board){

        $log.log('[boardController] Start.');

        let ctrl = this;

        ctrl.init = () => {
            ctrl.boards = [
                {
                    header: 'A',
                    detail: 'Aaa',
                    reading: 0,
                    favorite: 0,
                    favoriteFromMe: false,
                    readed: false,
                    flag: false
                },
                {
                    header: 'B',
                    detail: 'Bbb',
                    reading: 1,
                    favorite: 1,
                    favoriteFromMe: true,
                    readed: true,
                    flag: false
                },
                {
                    header: 'C',
                    detail: 'Ccc',
                    reading: 2,
                    favorite: 2,
                    favoriteFromMe: true,
                    readed: true,
                    flag: true
                }
            ];

            ctrl.checkFilter = {
                general: {
                    status: true,
                    flex: 50
                },
                love: {
                    status: true,
                    flex: 50
                },
                entertainment: {
                    status: true,
                    flex: 50
                },
                sport: {
                    status: true,
                    flex: 50
                },
                hmongNewYear: {
                    status: true,
                    flex: 50
                }
            }
        };

        ctrl.saveFlag = (index) => {
            ctrl.boards[index].flag = !ctrl.boards[index].flag;
        };

        ctrl.saveFavorite = (index) => {
            ctrl.boards[index].favoriteFromMe = !ctrl.boards[index].favoriteFromMe;
            if(ctrl.boards[index].favoriteFromMe) {
                ctrl.boards[index].favorite = ctrl.boards[index].favorite + 1;
            } else {
                ctrl.boards[index].favorite = ctrl.boards[index].favorite - 1;
            }
        };

        $log.log('[boardController] End.');

    }];

export {
    boardController
}
